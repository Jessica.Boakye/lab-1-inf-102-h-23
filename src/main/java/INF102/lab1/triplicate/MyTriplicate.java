package INF102.lab1.triplicate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {

        Map <T, Integer> triplicat = new HashMap<>();

        for (T obj : list){
            Integer count = triplicat.get(obj);
            if(count == null){
                triplicat.put(obj, 1);
            }
            else{
                triplicat.put(obj, ++count);
            }
        }

        Set<Entry<T, Integer>> entrySet = triplicat.entrySet();
        for (Entry<T, Integer> entry : entrySet){
            if(entry.getValue() > 2){
                return entry.getKey();
            }
        } 
        return null;
    }
}

 /*
        
        Collections.sort(list);
        for(int i = 0; i<list.size()-2; i++){
            if(list.get(i).equals(list.get(i+1)) && list.get(i).equals(list.get(i+2))){
                System.out.println(list.get(i));
                return list.get(i);
            }
        }
        return null;
    } 
    */
